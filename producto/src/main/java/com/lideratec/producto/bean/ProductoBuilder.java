package com.lideratec.producto.bean;

import com.lideratec.producto.modelo.ProductoEntidad;

public class ProductoBuilder {

	private ProductoBuilder(Producto producto) {}
	
	public static class Builder{
		
		private ProductoEntidad prod;
		
		public Builder(String titulo, Double precio, Integer stock) {
			prod.setTitulo(titulo);
			prod.setPrecio(precio);
			prod.setStock(stock);			
		}
		
		public Builder setDescripcion(String descripcion){
			prod.setDescripcion(descripcion);
			return this;		
		}

		public Builder setCaracteristica(String caracteristica){
			prod.setCaracteristica(caracteristica);
			return this;
		}
		
		public Builder setFoto(String foto){
			prod.setFoto(foto);
			return this;
		}
		
		public Builder setEstado(Integer estado){
			prod.setEstado(estado);
			return this;
		}
		
		public  ProductoEntidad build(){
			return prod;
		}
	}
}
