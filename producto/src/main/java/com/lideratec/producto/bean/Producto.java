package com.lideratec.producto.bean;

public class Producto extends Base{
	
	private static final long serialVersionUID = -2993892329783160390L;

	
	public enum Estado {
		SUCCESS(0),
		ERROR(1);
		
	    private final Integer value;

		private Estado(Integer value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}
	
	private Integer id;
	private String titulo;
	private String descripcion;
	private String caracteristica;
	private Double precio;
	private String foto;
	private Integer estado;
	private Integer Stock;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCaracteristica() {
		return caracteristica;
	}
	public void setCaracteristica(String caracteristica) {
		this.caracteristica = caracteristica;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	public Integer getStock() {
		return Stock;
	}
	public void setStock(Integer stock) {
		Stock = stock;
	}

}
