package com.lideratec.producto.bean;

import java.io.Serializable;

public class Base implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
	private String mensaje;
	
	public Base() {}
	
	public Base(Integer codigo, String mensaje) {
		this.codigo = codigo;
		this.mensaje = mensaje;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public String getMensaje() {
		return mensaje;
	}

}
