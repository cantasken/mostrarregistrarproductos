package com.lideratec.producto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lideratec.producto.bean.Producto;
import com.lideratec.producto.bean.Producto.Estado;
import com.lideratec.producto.servicio.ProductoFactory;
import com.lideratec.producto.servicio.TemplateAbstract;
import com.lideratec.producto.util.Mensajeria.Operacion;

@CrossOrigin(origins = "https://734f6e56393a.ngrok.io")
@Controller
@RequestMapping("/usuario")
public class ProductoController {

	@Autowired
	private ProductoFactory producto;
	
	@Autowired
	private TemplateAbstract plantilla;
	
	@PostMapping(path="/")
	public @ResponseBody ResponseEntity<HttpStatus> nuevoProducto(@RequestBody Producto prd) {
		
		plantilla.setOperacion(Operacion.REGISTRAR.name());
		return new ResponseEntity<HttpStatus>(plantilla.respuesta(plantilla.crearPlantilla(prd)));

//		if(producto.crearAvanzado().registrar(prd).getCodigo() == Estado.SUCCESS.getValue())
//			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
//		else
//			return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping(path="/")
	public @ResponseBody ResponseEntity<List<Producto>> listarProductos(){
		
		return new ResponseEntity<List<Producto>>(producto.crearAvanzado().listar(), HttpStatus.OK );
	}
	
	@DeleteMapping(path="/")
	public @ResponseBody ResponseEntity<HttpStatus> borrarProducto(Integer id){
		
		if(producto.crearAvanzado().borrar(id).getCodigo() == Estado.SUCCESS.getValue())
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		else
			return new ResponseEntity<HttpStatus>(HttpStatus.BAD_REQUEST);
	}
}
