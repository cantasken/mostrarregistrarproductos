package com.lideratec.producto.servicio;

import com.lideratec.producto.bean.Base;
import com.lideratec.producto.bean.Producto;

public interface IProductoAvanzado extends IProductoBasico {

	Base registrar(Producto producto);
	Base actualizar(Producto producto);
	
	
}
