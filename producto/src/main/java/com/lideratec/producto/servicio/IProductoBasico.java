package com.lideratec.producto.servicio;

import java.util.List;

import com.lideratec.producto.bean.Base;
import com.lideratec.producto.bean.Producto;

public interface IProductoBasico {
	
	List<Producto> listar();
	Base borrar(Integer id);

}
