package com.lideratec.producto.servicio.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lideratec.producto.bean.Base;
import com.lideratec.producto.bean.Producto;
import com.lideratec.producto.bean.Producto.Estado;
import com.lideratec.producto.dao.ProductoRepositorio;
import com.lideratec.producto.modelo.ProductoEntidad;
import com.lideratec.producto.servicio.IProductoBasico;
import com.lideratec.producto.util.Mensajeria;

public class ProductoBasico implements IProductoBasico {

	Logger logger = LogManager.getLogger(ProductoSoftware.class);

	@Autowired
	private ProductoRepositorio repositorio;

	@Autowired
	private Mensajeria mensaje;
	
	@Override
	public List<Producto> listar() {
	
		List<Producto> lista = new ArrayList<Producto>();
		
		try {
			
			List<ProductoEntidad> build = repositorio.findAll();
			lista = build.stream()
				.map( p -> {
					Producto prod = new Producto();
					prod.setId(p.getId());
					prod.setTitulo(p.getTitulo());
					prod.setDescripcion(p.getDescripcion());
					prod.setEstado(p.getEstado());
					prod.setCaracteristica(p.getCaracteristica());
					prod.setFoto(p.getFoto());
					prod.setStock(p.getStock());
					prod.setPrecio(p.getPrecio());
					return prod;
				})
				.collect(Collectors.toList());
			
			logger.info(mensaje.SUCCESS);

		} catch (Exception e) {
			
			logger.info(mensaje.ERROR, e);

		}
		
		return lista;
		
	}

	@Override
	public Base borrar(Integer id) {
		
		try {
			repositorio.deleteById(id);
			
			logger.info(mensaje.SUCCESS);
			
			return new Base(Estado.SUCCESS.getValue(), mensaje.SUCCESS);

		} catch (Exception e) {
			
			logger.info(mensaje.ERROR, e);
			
			return new Base(Estado.ERROR.getValue(), mensaje.ERROR);

		}
		
	}

}
