package com.lideratec.producto.servicio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lideratec.producto.servicio.IProductoAvanzado;
import com.lideratec.producto.servicio.ProductoFactory;

@Service
public class ProductoSoftwareFactory extends ProductoFactory{

	@Autowired
	private ProductoSoftware producto;
	
	@Override
	protected IProductoAvanzado crearProductoAvanzado() {
		return producto;
	}

}
