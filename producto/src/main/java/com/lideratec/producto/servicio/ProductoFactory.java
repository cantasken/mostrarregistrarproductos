package com.lideratec.producto.servicio;

import org.springframework.stereotype.Component;

@Component
public abstract class ProductoFactory  {
	
	public IProductoAvanzado crearAvanzado(){
		
		IProductoAvanzado producto = crearProductoAvanzado();
		
		return producto;
	}
	
	protected abstract IProductoAvanzado crearProductoAvanzado();
	
}
