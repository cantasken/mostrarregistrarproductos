package com.lideratec.producto.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.lideratec.producto.bean.Base;
import com.lideratec.producto.bean.Producto;
import com.lideratec.producto.bean.Producto.Estado;
import com.lideratec.producto.util.Mensajeria.Operacion;

@Component
public class TemplateAbstract {
	
	@Autowired
	private ProductoFactory producto;

	private String operacion;
	

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public Base crearPlantilla(Producto prod){
		
		if(Operacion.REGISTRAR.equals(this.operacion)){
			return producto.crearAvanzado().registrar(prod);
		}
		else if(Operacion.BORRAR.equals(this.operacion)){
			return producto.crearAvanzado().borrar(prod.getCodigo());
		}	
		else{
			return null;
		}
	}
	
	public HttpStatus respuesta(Base base){
		
		if(base.getCodigo() == Estado.SUCCESS.getValue()){
			return HttpStatus.OK;
		}
		else if(base.getCodigo() == Estado.ERROR.getValue()){
			return HttpStatus.BAD_REQUEST;
		}else{
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
		
	}

}
