package com.lideratec.producto.servicio.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.lideratec.producto.bean.Base;
import com.lideratec.producto.bean.Producto;
import com.lideratec.producto.bean.ProductoBuilder;
import com.lideratec.producto.bean.Producto.Estado;
import com.lideratec.producto.dao.ProductoRepositorio;
import com.lideratec.producto.modelo.ProductoEntidad;
import com.lideratec.producto.servicio.IProductoAvanzado;
import com.lideratec.producto.util.Mensajeria;

public class ProductoDisenio extends ProductoBasico implements IProductoAvanzado {
	
	Logger logger = LogManager.getLogger(ProductoSoftware.class);
	
	@Autowired
	private ProductoRepositorio repositorio;
	
	@Autowired
	private Mensajeria mensaje;
	
	@Override
	public Base registrar(Producto producto) {
		
		try {
			
			ProductoEntidad build = new ProductoBuilder.Builder(
					producto.getTitulo(),
					producto.getPrecio(),
					producto.getStock())
					.build();
			repositorio.save(build);
			
			logger.info(mensaje.SUCCESS);

			return new Base(Estado.SUCCESS.getValue(), mensaje.SUCCESS);
			
		} catch (Exception e) {
			logger.info(mensaje.ERROR, e);

			return new Base(Estado.ERROR.getValue(), mensaje.ERROR + e.getMessage());  
		}
	}

	@Override
	public Base actualizar(Producto producto) {
		// TODO Auto-generated method stub
		return null;
	}

}
