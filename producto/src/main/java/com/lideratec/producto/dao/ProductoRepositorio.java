package com.lideratec.producto.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lideratec.producto.modelo.ProductoEntidad;

public interface ProductoRepositorio extends JpaRepository<ProductoEntidad, Integer>{

}
